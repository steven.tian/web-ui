import React from 'react';
import { useNavigate } from "react-router-dom";

import {DOMAIN} from '../util/auth'

export default function Login() {

    const navigate = useNavigate();
    const submit = () => {
        let credential = document.getElementById("check").value;
        fetch(DOMAIN+"product/check?credential=" + credential)
            .then((response) => response.json())
            .then((data) => {
                if (data.url === 'index') {
                   
                    let idIdx = window.location.search.indexOf("id");
                   
                    let id =window.location.search.substring(idIdx+3)
                   
                    if (id.indexOf('*') !== -1) {
                        navigate("/bedSetRst?id=" + id);
                    } else {
                        navigate("/barcode?id=" + id);
                    }

                } else if (data.url === 'admin') {
                    sessionStorage.setItem('user-token', data.token);
                    navigate("/");
                } else if (data.url === 'sales') {
                    navigate("/sales");
                } else {
                    navigate("/error");
                }

            })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        submit();
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label >密碼:</label>
                <input type="password" id="check" name="credential" />
                <input type='submit' />
            </form>

        </div>
    )
}