import { useMemo, useState, useEffect } from 'react';
import React from 'react';
import {
  MRT_EditActionButtons,
  MaterialReactTable,
  // createRow,
  type MRT_ColumnDef,
  type MRT_Row,
  type MRT_TableOptions,
  useMaterialReactTable,
} from 'material-react-table';

import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Input,
  Tooltip,
} from '@mui/material';
import {
  QueryClient,
  QueryClientProvider,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import { DOMAIN } from '../util/auth'

const Example = () => {
  const [validationErrors, setValidationErrors] = useState<
    Record<string, string | undefined>
  >({});

  const columns = useMemo<MRT_ColumnDef<Product>[]>(
    () => [
      {
        accessorKey: 'id',
        header: 'id',
        enableHiding: true,
        enableColumnActions: false,
        enableEditing: false
      },
      {
        accessorKey: 'col1',
        header: '1',
        enableColumnActions: false
      },
      {
        accessorKey: 'col2',
        header: '2',
        enableColumnActions: false
      }, {
        accessorKey: 'col3',
        header: '3',
        enableColumnActions: false
      }, {
        accessorKey: 'col4',
        header: '4',
        enableColumnActions: false
      }, {
        accessorKey: 'col5',
        header: '5',
        enableColumnActions: false
      }, {
        accessorKey: 'col6',
        header: '6',
        enableColumnActions: false
      }, {
        accessorKey: 'col7',
        header: '7',
        enableColumnActions: false
      }, {
        accessorKey: 'col8',
        header: '8',
        enableColumnActions: false
      },
    ],
    [validationErrors],
  );

  //call CREATE hook
  const { mutateAsync: createUser, isPending: isCreatingUser } =
    useCreateUser();
  //call READ hook
  const {
    data: fetchedUsers = [],
    isError: isLoadingUsersError,
    isFetching: isFetchingUsers,
    isLoading: isLoadingUsers,
  } = useGetUsers();
  //call UPDATE hook
  const { mutateAsync: updateUser, isPending: isUpdatingUser } =
    useUpdateUser();
  //call DELETE hook
  const { mutateAsync: deleteUser, isPending: isDeletingUser } =
    useDeleteUser();

  //CREATE action
  const handleCreateUser: MRT_TableOptions<Product>['onCreatingRowSave'] = async ({
    values,
    table,
  }) => {

    setValidationErrors({});
    await createUser(values as Product);
    table.setCreatingRow(null); //exit creating mode
  };

  //UPDATE action
  const handleSaveUser: MRT_TableOptions<Product>['onEditingRowSave'] = async ({
    values,
    table,
  }) => {

    setValidationErrors({});
    await updateUser(values as Product);
    table.setEditingRow(null); //exit editing mode
  };

  //DELETE action
  const openDeleteConfirmModal = (row: MRT_Row<Product>) => {
    if (window.confirm('確定要砍掉這筆資料 ?')) {
      deleteUser(row.original.id);
    }
  };

  const table = useMaterialReactTable({
    columns,
    data: fetchedUsers,
    createDisplayMode: 'modal', //default ('row', and 'custom' are also available)
    editDisplayMode: 'modal', //default ('row', 'cell', 'table', and 'custom' are also available)
    enableEditing: true,
    getRowId: (row) => row.id?.toString(),
    muiToolbarAlertBannerProps: isLoadingUsersError
      ? {
        color: 'error',
        children: 'Error loading data',
      }
      : undefined,
    muiTableContainerProps: {
      sx: {
        minHeight: '500px',
      },
    },
    onCreatingRowCancel: () => setValidationErrors({}),
    onCreatingRowSave: handleCreateUser,
    onEditingRowCancel: () => setValidationErrors({}),
    onEditingRowSave: handleSaveUser,
    //optionally customize modal content
    renderCreateRowDialogContent: ({ table, row, internalEditComponents }) => (
      <>
        <DialogTitle variant="h3"></DialogTitle>
        <DialogContent
          sx={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}
        >
          {internalEditComponents} {/* or render custom edit components here */}
        </DialogContent>
        <DialogActions>
          <MRT_EditActionButtons variant="text" table={table} row={row} />
        </DialogActions>
      </>
    ),
    //optionally customize modal content
    renderEditRowDialogContent: ({ table, row, internalEditComponents }) => (
      <>
        <DialogTitle variant="h3"></DialogTitle>
        <DialogContent
          sx={{ display: 'flex', flexDirection: 'column', gap: '1.5rem' }}
        >
          {internalEditComponents} {/* or render custom edit components here */}
        </DialogContent>
        <DialogActions>
          <MRT_EditActionButtons variant="text" table={table} row={row} />
        </DialogActions>
      </>
    ),
    renderRowActions: ({ row, table }) => (
      <Box sx={{ display: 'flex', gap: '1rem' }}>
        <Tooltip title="Edit">
          <IconButton onClick={() => table.setEditingRow(row)}>
            <EditIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Delete">
          <IconButton color="error" onClick={() => openDeleteConfirmModal(row)}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </Box>
    ),
    renderTopToolbar: ({ table }) => (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Button
          variant="contained"
          onClick={() => {
            table.setCreatingRow(true); //simplest way to open the create row modal with no default values
            //or you can pass in a row object to set default values with the `createRow` helper function
            // table.setCreatingRow(
            //   createRow(table, {
            //     //optionally pass in default values for the new row, useful for nested data or other complex scenarios
            //   }),
            // );
          }}
        >
          建立資料
        </Button>
        <Input id={'fileinput'} type='file' onChange={upload}>
        </Input>
      </div>
    ),
    state: {
      isLoading: isLoadingUsers,
      isSaving: isCreatingUser || isUpdatingUser || isDeletingUser,
      showAlertBanner: isLoadingUsersError,
      showProgressBars: isFetchingUsers,
    },
    localization: { cancel: '取消', save: '儲存', actions: '動作' },
    initialState: { columnVisibility: { id: false } }, //hide firstName column by default
  });

  return <MaterialReactTable table={table} />;
};

//CREATE hook (post new user to api)
function useCreateUser() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (user: Product) => {
      //send api update request here
      //await new Promise((resolve) => setTimeout(resolve, 1000)); //fake api call
      return Promise.resolve();
    },
    //client side optimistic update
    onMutate: (newUserInfo: Product) => {
      queryClient.setQueryData(
        ['products'],
        (prevUsers: any) =>
          [
            ...prevUsers,
            {
              ...newUserInfo,
              id: Date.now()

            },
          ] as Product[],
      );
    },
    // onSettled: () => queryClient.invalidateQueries({ queryKey: ['products'] }), //refetch products after mutation, disabled for demo
  });
}

// This will upload the file after having read it
const upload = () => {
  const input: any = document.getElementById('fileinput');
  let file: any = input?.files[0];
  let loadNumber: any = document.getElementById('queryNumber');
  let loadBtn = document.getElementById('load');
  fetch(DOMAIN + 'product/upload', { // Your POST endpoint
    method: 'POST',
    headers: {
      // Content-Type may need to be completely **omitted**
      // or you may need something
      //"Content-Type": "application/vnd.ms-excel"
    },
    body: file // This is your file object
  }).then(
    response => response.json() // if the response is a JSON object
  ).then(
    success => {
      alert(success.result);// Handle the success response object
      input.value = null;
      loadNumber.value = success.queryNumber;
      loadBtn?.click();
    }
  ).catch(
    error => console.log(error) // Handle the error response object
  );

};



//READ hook (get products from api)
function useGetUsers() {
  return useQuery<Product[]>({
    queryKey: ['products'],
    queryFn: async () => {
      //send api request here
      //await new Promise((resolve) => setTimeout(resolve, 1000)); //fake api call
      return Promise.resolve([]);
    },
    refetchOnWindowFocus: false,
  });
}

//UPDATE hook (put user in api)
function useUpdateUser() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (user: Product) => {
      //send api update request here
      //await new Promise((resolve) => setTimeout(resolve, 1000)); //fake api call
      return Promise.resolve();
    },
    //client side optimistic update
    onMutate: (newUserInfo: Product) => {
      queryClient.setQueryData(
        ['products'],
        (prevUsers: any) =>
          prevUsers?.map((prevUser: Product) =>
            prevUser.id === newUserInfo.id ? newUserInfo : prevUser,
          ),
      );
    },
    // onSettled: () => queryClient.invalidateQueries({ queryKey: ['products'] }), //refetch products after mutation, disabled for demo
  });
}

//DELETE hook (delete user in api)
function useDeleteUser() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (col1: number) => {
      //send api update request here
      await new Promise((resolve) => setTimeout(resolve, 1000));
      return Promise.resolve();
    },
    //client side optimistic update
    onMutate: (id: number) => {
      queryClient.setQueryData(
        ['products'],
        (prevUsers: any) =>
          prevUsers?.filter((user: Product) => user.id !== id),
      );
    },
    // onSettled: () => queryClient.invalidateQueries({ queryKey: ['products'] }), //refetch products after mutation, disabled for demo
  });
}

const queryClient = new QueryClient();
const handleSubmit = async (desc: string, name: string, number: string, comment: string, e: any) => {

  let data: any = { partial: { name: name, number: number, comment: comment } }
  data.products = queryClient.getQueriesData({ queryKey: ['products'] })[0][1];
  data.description = desc;
  console.log(data);
  const response = await fetch(DOMAIN + 'product/bedSet', {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  const rep = await response.json();
  alert(JSON.stringify(rep.result));
}

function downloadFile(number: string) {

  if (!number) {
    alert("請先輸入編號 !");
    return;
  }

  fetch(DOMAIN + 'product/genQRcode/' + number, { method: 'get' })
    .then(res => res.blob())
    .then(res => {
      const aElement = document.createElement('a');
      aElement.setAttribute('download', number + ".jpg");
      const href = URL.createObjectURL(res);
      aElement.href = href;
      aElement.setAttribute('target', '_blank');
      aElement.click();
      URL.revokeObjectURL(href);
    });
  alert("下載完成")

};

const ExampleWithProviders = () => {

  const [name, setName] = useState('');
  const [number, setNumber] = useState('');
  const [comment, setComment] = useState('');
  const [desc, setDesc] = useState('');
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get('id');
  const [viewNumber, setViewNumber] = useState(id ? id : '');
  useEffect(() => {
    if (viewNumber) {
      viewProduct();
    }
  }, []);

  const viewProduct = () => {

    /*
    let obj: any = document.getElementById('queryNumber')
    const viewNumber = obj.value;
*/
    fetch(DOMAIN + 'product/bedSet/' + viewNumber)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        if (data) {
          setName(data.partial?.name);
          setNumber(data.partial?.number);
          setComment(data.partial?.comment);
          setDesc(data?.description);
          queryClient.setQueryData(['products'], data.products);
        }

      });
  }

  const clearAll = async () => {
    if (!number) {
      alert("請先輸入編號 !");
      return;
    }
    try {


      const response = await fetch(DOMAIN + 'product/bedSet/' + number, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        }
      });

      const rep = await response.json();
      alert(JSON.stringify(rep.result));
      setName('');
      setNumber('');
      setComment('');
      setDesc('');
      queryClient.setQueryData(['products'], []);
    } catch (error) {
      console.error("Error:", error);
    }
  }

  return (
    <div>
      <div>
        <label >編號:</label>
        <Input id="queryNumber" type="text" name="number" onChange={e=>setViewNumber(e.target.value)}/>
        <button id="load" onClick={viewProduct}>載入</button>
      </div>

      <br />
      <br />
      <div>
        <label>品名:<br />
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </label>
        <br />
        <label>編號:<br />
          <input
            id="number"
            value={number}
            onChange={(e) => {
              setNumber(e.target.value)

            }}
          />
        </label> <br />
        <label>備註:<br />
          <input
            id="comment"
            value={comment}
            onChange={(e) => {
              setComment(e.target.value);

            }}
          />
        </label></div>
      <br />
      <br />
      <QueryClientProvider client={queryClient}>
        <Example />
        <div>
          <br />
          <label>說明:<br />
            <textarea id="description" rows={20} cols={50} value={desc} onChange={(e) => {
              setDesc(e.target.value);
            }}
            />
          </label>
          <br />

        </div>
        <button style={{ color: 'green' }} onClick={(e) => handleSubmit(desc, name, number, comment, e)}>存檔</button>
        <button style={{ color: 'red' }} onClick={clearAll}>刪除</button>
        <button onClick={(e) => downloadFile(number)}>產生QR code</button>
      </QueryClientProvider>



    </div>
  )
};

export type MyPartial = {
  name: string,
  comment: string,
  number: string
}

export type Product = {
  id: number;
  col1: string;
  col2: string;
  col3: string;
  col4: string;
  col5: string;
  col6: string;
  col7: string;
  col8: string;
};

export default ExampleWithProviders;
