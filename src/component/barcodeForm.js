import React from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import {DOMAIN} from '../util/auth'
export default function BarcodeForm() {
    console.log(window.location.search);
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    const [name, setName] = useState('');
    const [number, setNumber] = useState('');
    const [price, setPrice] = useState('');
    const [spec, setSpec] = useState('');
    const [comment, setComment] = useState('');
    const [skinLoc, setSkinLoc] = useState('');
    const [description, setDescription] = useState('');
    const navigate = useNavigate();
    
    useEffect(() => {
        
        fetch(DOMAIN+'product/barcode/'+id)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                if(data){
                    setName(data.name);
                    setNumber(data.number);
                    setPrice(data.price);
                    setSpec(data.spec);
                    setComment(data.comment);
                    setSkinLoc(data.skinLoc);
                    setDescription(data.description)
                }else{
                    navigate("/error");   
                }
                
            });
    }, []);

    return (
        <div>
            <form >
                <label>品名:<br />
                    <input
                        type="text"
                        id="name"
                        value={name}
                    />
                </label>
                <br />
                <label>編號:<br />
                    <input
                        id="number"
                        value={number}
                    />
                </label>
                <br />
                <label>價格:<br />
                    <input
                        id="price"
                        value={price}
                    />
                </label>
                <br />
                <label>規格:<br />
                    <input
                        id="spec"
                        value={spec}
                    />
                </label>
                <br />
                <label>備註:<br />
                    <input
                        id="comment"
                        value={comment}
                    />
                </label>
                <br />
                <label>皮版位置:<br />
                    
                    <textarea id="skinLoc" rows="6" cols="21" value={skinLoc} onChange={(e) => setSkinLoc(e.target.value)} />
                </label>
                <br />
                <label>說明:<br />
                    <textarea id="description" rows="20" cols="50" value={description} />
                </label>
                <br />
                <br />
            </form>


        </div>
    )
}