import React from 'react';
import { useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import {DOMAIN} from '../util/auth'
import { Link} from "react-router-dom";

export default function BasicTable() {
   
    const [rows, setRows] = useState([]);
    const [viewNumber, setViewNumber] = useState('');

    const processNumber = (str) => {
        if(str){
            let newStr = '';
            if(str.includes(",") && !str.includes('元')){
                newStr = str + '元';
                return newStr;
            }else if(!str.includes(",")){
                newStr = str
                .replace(/(.)(.{12})$/, '$1,$2') // add comma to billion
                .replace(/(.)(.{9})$/, '$1,$2')  // add comma to million
                .replace(/(.)(.{6})$/, '$1,$2') // add comma to thousand
                .replace(/(.)(.{3})$/, '$1,$2'); // add comma to thousand
                return newStr += '元';
            }
        }      
         
        return str;
    }

    const  viewProduct = () => {
        fetch(DOMAIN + 'product/query/' + viewNumber)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                
                setRows(data);
            });
    }

    const determineLink = (number) => {
        if(number.indexOf('*') !== -1){
            return ("/nothing-here?id=" + number);
        }else{
            return ("/dashboard?id=" + number);
        }

    };

    return (

        <div>
            <label >編號:</label>
            <input type="text" name="number" value={viewNumber} onChange={(e) => setViewNumber(e.target.value)} />
            <button onClick={viewProduct}>載入</button>

            <TableContainer style={{ width: 400 }}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>編號</TableCell>
                            <TableCell align="center">品名</TableCell>
                            <TableCell align="center">價格</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows && rows.map && rows.map((row) => (
                            <TableRow
                                key={row.number}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    <Link to={determineLink(row.number)}>{row.number}</Link>
                                </TableCell>
                                <TableCell align="center">{row.name}</TableCell>
                                <TableCell align="center">{processNumber(row.price)}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>

    );
}