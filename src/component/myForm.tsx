import React from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { DOMAIN } from '../util/auth'
export function formatPrice(str: string) {

    if (str.match(/^\d+(\.\d*)?$/)) {
        str = str
            .replace(/(.)(.{12})$/, '$1,$2') // add comma to billion
            .replace(/(.)(.{9})$/, '$1,$2')  // add comma to million
            .replace(/(.)(.{6})$/, '$1,$2') // add comma to thousand
            .replace(/(.)(.{3})$/, '$1,$2'); // add comma to thousand
        str += '元';
        return str;
    } else {
        return ''; // return empty string if format is incorrect
    }
}

export default function MyForm() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    const [name, setName] = useState('');
    const [number, setNumber] = useState('');
    const [viewNumber, setViewNumber] = useState(id ? id : '');
    const [price, setPrice] = useState('');
    const [spec, setSpec] = useState('');
    const [comment, setComment] = useState('');
    const [skinLoc, setSkinLoc] = useState('');
    const [description, setDescription] = useState('');

    useEffect(() => {
        if (viewNumber) {
            viewProduct();
        }
    }, []);

    const viewProduct = () => {
        console.log("id==>" + viewNumber);
        fetch(DOMAIN + 'product/barcode/' + viewNumber)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log("data==>" + data);
                if (data) {
                    setName(data.name);
                    setNumber(data.number);
                    setPrice(data.price);
                    setSpec(data.spec);
                    setComment(data.comment);
                    setSkinLoc(data.skinLoc);
                    setDescription(data.description)
                }

            });
    }

    function downloadFile() {

        if (!number) {
            alert("請先輸入編號 !");
            return;
        }
        fetch(DOMAIN + 'product/genQRcode/' + number, { method: 'get' })
            .then(res => res.blob())
            .then(res => {
                const aElement = document.createElement('a');
                aElement.setAttribute('download', number + ".jpg");
                const href = URL.createObjectURL(res);
                aElement.href = href;
                aElement.setAttribute('target', '_blank');
                aElement.click();
                URL.revokeObjectURL(href);
            });
        alert("下載完成")
    };

    async function deleteNumber() {

        if (!number) {
            alert("請先輸入編號 !");
            return;
        }
        try {
            const response = await fetch(DOMAIN + 'product/delete/' + number, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                }
            });

            const rep = await response.json();
            alert(JSON.stringify(rep.result));
            setName('');
            setNumber('');
            setPrice('');
            setSpec('');
            setComment('');
            setSkinLoc('');
            setDescription('')
        } catch (error) {
            console.error("Error:", error);
        }
    };

    const handleSubmit = async (e: any) => {
        e.preventDefault();
        postJSON();
    }

    async function postJSON() {
        try {
            let data = { number, name, price, spec, comment, skinLoc, description };
            data.number = number;
            data.name = name
            data.price = price;
            data.spec = spec;
            data.comment = comment;
            data.skinLoc = skinLoc;
            data.description = description;

            const response = await fetch(DOMAIN + 'product/add', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
            });

            const rep = await response.json();
            alert(JSON.stringify(rep.result));

        } catch (error) {
            console.error("Error:", error);
        }
    }

    return (

        <div>
            <div>
                <label >編號:</label>
                <input type="text" name="number" value={viewNumber} onChange={(e) => setViewNumber(e.target.value)} />
                <button onClick={viewProduct}>載入</button>
            </div>
            <br />
            <br />
            <form>
                <label>品名:<br />
                    <input
                        type="text"
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </label>
                <br />
                <label>編號:<br />
                    <input
                        id="number"
                        value={number}
                        onChange={(e) => setNumber(e.target.value)}
                    />
                </label>
                <br />
                <label>價格:<br />
                    <input
                        id="price"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                        onBlur={(e) => {
                            let _p = formatPrice(e.target.value);
                            setPrice(_p);
                        }

                        }
                    />
                </label>
                <br />
                <label>規格:<br />
                    <input
                        id="spec"
                        value={spec}
                        onChange={(e) => setSpec(e.target.value)}
                    />
                </label>
                <br />
                <label>備註:<br />
                    <input
                        id="comment"
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                    />
                </label>
                <br />
                <label>皮版位置:<br />

                    <textarea id="skinLoc" rows={6} cols={21} value={skinLoc} onChange={(e) => setSkinLoc(e.target.value)} />
                </label>
                <br />
                <label>說明:<br />
                    <textarea id="description" rows={20} cols={50} value={description} onChange={(e) => setDescription(e.target.value)} />
                </label>
                <br />
                <br />
                <button style={{ color: 'green' }} onClick={handleSubmit}>存檔
                </button>
                <br />
            </form>
            <button style={{ color: 'red' }} onClick={deleteNumber}>刪除</button>
            <button onClick={downloadFile}>產生QR code</button>

        </div>

    );



}