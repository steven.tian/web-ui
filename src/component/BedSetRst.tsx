import { useEffect, useMemo, useState } from 'react';
import React from 'react';
import {
    MaterialReactTable,
    // createRow,
    type MRT_ColumnDef,
    useMaterialReactTable,
} from 'material-react-table';

import {
    QueryClient,
    QueryClientProvider,
    useMutation,
    useQuery,
    useQueryClient,
} from '@tanstack/react-query';

import {DOMAIN} from '../util/auth'

const Example = () => {
    const [validationErrors, setValidationErrors] = useState<
        Record<string, string | undefined>
    >({});

    const columns = useMemo<MRT_ColumnDef<Product>[]>(
        () => [
            {
                accessorKey: 'id',
                header: 'id',
                enableHiding: true,
                enableColumnActions: false,
                enableEditing: false
            },
            {
                accessorKey: 'col1',
                header: '1',
                enableColumnActions: false
            },
            {
                accessorKey: 'col2',
                header: '2',
                enableColumnActions: false
            }, {
                accessorKey: 'col3',
                header: '3',
                enableColumnActions: false
            }, {
                accessorKey: 'col4',
                header: '4',
                enableColumnActions: false
            }, {
                accessorKey: 'col5',
                header: '5',
                enableColumnActions: false
            }, {
                accessorKey: 'col6',
                header: '6',
                enableColumnActions: false
            }, {
                accessorKey: 'col7',
                header: '7',
                enableColumnActions: false
            }, {
                accessorKey: 'col8',
                header: '8',
                enableColumnActions: false
            },
        ],
        [validationErrors],
    );

    //call READ hook
    const {
        data: fetchedUsers = [],
        isError: isLoadingUsersError,
        isFetching: isFetchingUsers,
        isLoading: isLoadingUsers,
    } = useGetUsers();

    const table = useMaterialReactTable({
        columns,
        data: fetchedUsers,
        getRowId: (row) => row.id?.toString(),
        muiToolbarAlertBannerProps: isLoadingUsersError
            ? {
                color: 'error',
                children: 'Error loading data',
            }
            : undefined,
        muiTableContainerProps: {
            sx: {
                minHeight: '500px',
            },
        },
        renderTopToolbar: ({ table }) => (
            <div></div>


        ),
        state: {
            isLoading: isLoadingUsers,
            showAlertBanner: isLoadingUsersError,
            showProgressBars: isFetchingUsers,
        },
        localization: { cancel: '取消', save: '儲存', actions: '動作' },
        initialState: { columnVisibility: { id: false } }, //hide firstName column by default
    });

    return <MaterialReactTable table={table} />;
};


//READ hook (get products from api)
function useGetUsers() {
    return useQuery<Product[]>({
        queryKey: ['products'],
        queryFn: async () => {
            const urlParams = new URLSearchParams(window.location.search);
            const id = urlParams.get('id');
            const products = fetch(DOMAIN + 'product/bedset/detail/' + id)
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    return data.products;
                });

            return Promise.resolve(products);
        },
        refetchOnWindowFocus: false,
    });
}


const Part = () => {
    const queryClient = useQueryClient();
    const result = useQuery<MyPartial>({
        queryKey: ['partial'],
        queryFn: async () => {
            const urlParams = new URLSearchParams(window.location.search);
            const id = urlParams.get('id');
            const partial = fetch(DOMAIN + 'product/bedset/master/' + id)
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    return data;
                });

            return Promise.resolve(partial);
        },
        refetchOnWindowFocus: false
    });
    return (
        <div>
            <label>品名:<br />
                <input
                    type="text"
                    id="name"
                    value={result.data?.name}
                    onChange={(e) => {
                        queryClient.setQueryData(['partial'],
                            {
                                number: result.data?.number,
                                name: e.target.value,
                                comment: result.data?.comment

                            }

                        )

                    }}
                />
            </label>
            <br />
            <label>編號:<br />
                <input
                    id="number"
                    value={result.data?.number}
                    onChange={(e) => {
                        queryClient.setQueryData(['partial'],
                            {
                                number: e.target.value,
                                name: result.data?.name,
                                comment: result.data?.comment

                            }

                        )

                    }}
                />
            </label> <br />
            <label>備註:<br />
                <input
                    id="comment"
                    value={result.data?.comment}
                    onChange={(e) => {
                        queryClient.setQueryData(['partial'],
                            {
                                number: result.data?.number,
                                name: result.data?.name,
                                comment: e.target.value
                            }

                        )

                    }}
                />
            </label></div>);
}

const Description = () => {
    const queryClient = useQueryClient();
    const result = useQuery<string>({
        queryKey: ['description'],
        queryFn: async () => {
            const urlParams = new URLSearchParams(window.location.search);
            const id = urlParams.get('id');
            const obj:any= await fetch(DOMAIN + 'product/bedset/desc/' + id)
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    return data;
                });
           
            return Promise.resolve(obj.description);
        },
        refetchOnWindowFocus: false
    });
    return (<div>
        <br />
        <label>說明:<br />
            <textarea id="description" rows={20} cols={50} value={result.data} onChange={(e) => {
                queryClient.setQueryData(['description'],
                    e.target.value
                )
            }} />
        </label>
        <br />

    </div>)
}
const queryClient = new QueryClient();

const BedSetRst = () => (

    //Put this with your other react-query providers near root of your app
    <QueryClientProvider client={queryClient}>
        <br />
        <Part />
        <br />
        <br />
        <Example />
        <Description />

    </QueryClientProvider>
);

export type MyPartial = {
    name: string,
    comment: string,
    number: string
}

export type Product = {
    id: number;
    col1: string;
    col2: string;
    col3: string;
    col4: string;
    col5: string;
    col6: string;
    col7: string;
    col8: string;
};

export default BedSetRst;
