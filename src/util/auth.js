export const DOMAIN = 'http://localhost:8080/';
//export const DOMAIN = 'http://barcode.eastasia.cloudapp.azure.com:8080/';

const checkUserToken = () => {   
    const userToken = sessionStorage.getItem('user-token');
    if (userToken && userToken === 'admin') {   
        return true;
    }
    return false;
}
export default checkUserToken;