import React from 'react';
import { useEffect } from 'react';
import './App.css';
import MyForm from './component/myForm';
import BarcodeForm from './component/barcodeForm';
import Error from './component/error';
import Login from './component/login';
import BedSetRst from './component/BedSetRst';
import { Routes, Route, Outlet, Link } from "react-router-dom";
import BasicTable from './component/basicTable';
import ExampleWithProviders from './component/BedSet';
import checkUserToken from './util/auth';
import { useNavigate } from "react-router-dom";

function Layout() {
  const navigate = useNavigate();
  let pathName = window.location.pathname;
  const nonAdminUrl = () =>{
    return (pathName.indexOf("barcode") !== -1) || (pathName.indexOf("bedSetRst") !==-1) ||(pathName.indexOf("login") !==-1) ||(pathName.indexOf("error") !==-1)
  }
  useEffect(() => {
    const userToken = sessionStorage.getItem('user-token');

    if (pathName.indexOf("barcode")!== -1 || pathName.indexOf("bedSetRst") !== -1 || userToken !== 'admin') {
      navigate("/login?"+window.location.search);
    }
    
  }, []);

  return (<div>          
      {!nonAdminUrl() && <nav>
        <ul>

          <li>
            <Link to="/about">查詢</Link>
          </li>
          <li>
            <Link to="/dashboard">沙發</Link>
          </li>
          <li>
            <Link to="/nothing-here">床組/衣櫃</Link>
          </li>
        </ul>
      </nav>}

      {!nonAdminUrl()  && <hr /> }

      {/* An <Outlet> renders whatever child route is currently active,
          so you can think about this <Outlet> as a placeholder for
          the child routes we defined above. */}
      <Outlet />
    </div>
  );
}

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<BasicTable />} />
        <Route path="about" element={<BasicTable />} />
        <Route path="dashboard" element={<MyForm />} />
        <Route path="nothing-here" element={<ExampleWithProviders />} />
        <Route path="/barcode/*" element={<BarcodeForm />}>
        </Route>
        <Route path="/bedSetRst/*" element={<BedSetRst />}>
        </Route>
        <Route path="/error" element={<Error />}>
        </Route>
        <Route path="/login" element={<Login/>}>
        </Route>
        {/* Using path="*"" means "match anything", so this route
                acts like a catch-all for URLs that we don't have explicit
                routes for. */}
        <Route path="*" element={<Login/>}>
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
